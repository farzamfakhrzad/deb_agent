#!/bin/bash
if [ -f /etc/os-release ]; then
    # freedesktop.org and systemd
    . /etc/os-release
    OS=$NAME
    VER=$VERSION_ID
elif type lsb_release >/dev/null 2>&1; then
    # linuxbase.org
    OS=$(lsb_release -si)
    VER=$(lsb_release -sr)
elif [ -f /etc/lsb-release ]; then
    # For some versions of Debian/Ubuntu without lsb_release command
    . /etc/lsb-release
    OS=$DISTRIB_ID
    VER=$DISTRIB_RELEASE
elif [ -f /etc/debian_version ]; then
    # Older Debian/Ubuntu/etc.
    OS=Debian
    VER=$(cat /etc/debian_version)
else
    # Fall back to uname, e.g. "Linux <version>", also works for BSD, etc.
    OS=$(uname -s)
    VER=$(uname -r)
fi
info=${OS}_${VER}
data_dir="$1"/data.txt
pkg_dir="$1"/pkg.txt
temp_dir="$1"/temp.txt
xml_dir="$1"/pkg.xml
dpkg-query -W -f='${Package} ${version} ${Architecture}  ${Maintainer}  \n'> "$pkg_dir"
awk '{
print"<Obj REFID=\"0\">"
print" <TNRef RefId=\"0\" />"
print" <MS>  "
print"  <S N=\"Name\">"$1"</S>"
print"  <S N=\"Version\">"$2"</S> "
print"  <S N=\"Vendor\">"$3""$4"</S> "
print" </MS> "
print"</Obj> "
}' "$pkg_dir"  > "$data_dir"
echo '<Objs Version="1.1.0.1" xmlns="http://schemas.microsoft.com/powershell/2004/04">' | cat - "$data_dir"> temp && mv temp "$data_dir"
echo '<?xml version="1.0"?>' | cat - "$data_dir"> temp && mv temp  "$data_dir"
sed -i -e 's/<noreply@realvnc.com>/a/g' "$data_dir"
echo "</Objs>" >> "$data_dir"

cat "$data_dir" > "$xml_dir"
