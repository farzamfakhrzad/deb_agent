#!/bin/bash
trap 'die' INT TERM
die() {
    echo "error: $*" >&2
    exit 255
}

dir_flag=true
origin=$PWDi
directory=/var/vms-agent


function sig_int(){
    echo "exiting..."
    kill -SIGINT $PID
    exit 255
    sudo dpkg --remove vms-agent
}

trap sig_int INT

virtualenv -p python3 "$directory"/env &
PID=$!
trap sig_int INT
wait $PID
. "$directory"/env/bin/activate

if [[ "$(which python3)" != "/var/vms-agent/env/bin/python3" ]]
    then
        echo "error: couldn't start virtualenv. install virtualenv: sudo apt-get install virtualenv"
        exit 1
fi

chmod 700 "$directory"/proj.sh

pip3 install chardet==3.0.4|| die 'Failed to install chardet-3.0.4'
pip3 install certifi==2019.3.9 || die 'Failed to install certifi-2019.9.11'
pip3 install urllib3==1.24.2  || die 'Failed to install urllib3-1.05.7'
pip3 install idna==2.8 || die 'Failed to install idna-2.7'
pip3 install requests==2.21.0 || die 'Failed to install requests-2.22.0'
pip3 install distro==1.4.0|| die 'Failed to install distro-1.4.0'
pip3 install pycryptodome || die 'Failed to install pycryptodome check system architecture or requirements.'
pip3 install "$directory"/vms_agent-2.1.2-py3-none-any.whl || die 'Failed to install vms-agent-2.1.2-core'
cp "$directory"/vms_agent.timer /etc/systemd/system/vms-agent.timer
cp "$directory"/vms_agent.service /etc/systemd/system/vms-agent.service
read -p "> Please Enter  The VMS Ip Address(Do Not Enter http or https): " s_ip
read -p "> Please Enter  The Device Ip Address(Do Not Enter http or https): " d_ip
read -p "> Please Enter Your VMS Username: " user
read -s -p "> Please Enter Your VMS Password: " pass
read -p "> Please Specify The Type Of Scan You Want To Take Place.(deep or fast): " s_type

if  [[ "$s_type" = "deep" ]] 
then
    echo deep scan initiated 
    cp  "$directory"/agent_start_deep.sh "$directory"/agent_start.sh

fi
if [[ "$s_type" = "fast" ]]
then
    echo fast scan initiated 
    cp  "$directory"/agent_start_fast.sh "$directory"/agent_start.sh
fi
chmod 700 "$directory"/agent_start.sh
touch "$directory"/cred.txt
chmod 600 "$directory"/cred.txt
echo $s_ip >>"$directory"/cred.txt
echo $d_ip >>"$directory"/cred.txt
echo $user >>"$directory"/cred.txt
echo $pass >>"$directory"/cred.txt

systemctl enable vms-agent.timer
systemctl start vms-agent.timer
systemctl enable vms-agent.service
systemctl start vms-agent.service
systemctl daemon-reload
