from __future__ import print_function
from xml.dom import minidom
import xml.etree.ElementTree as ET
import socket
import platform
import os
import datetime
import atexit
from os import path
from json import dumps, loads
import re
from glob import glob
from os import getcwd, chdir
import requests
import sys
import logging.handlers
import base64
import json
from Crypto.Cipher import AES
import distro
import urllib3
from urllib.parse import urljoin

class agent:

    def encrypt(self, raw, pad):
        private_key = b'\xe9:\x17\x8d\xd9\x9a\xbf0\xb6\xc1\t\x93\x12|0+1\x9b\xfaU}+\x06\xa1\xe9\xe7\x87\r\xe6\x15\x9d\x01'
        raw = pad(raw)
        iv = 16 * '\x00'.encode()
        cipher = AES.new(private_key, AES.MODE_CBC, iv)
        return base64.b64encode(iv + cipher.encrypt(raw.encode()))

    def decrypt(self, enc, unpad):
        private_key = b'\xe9:\x17\x8d\xd9\x9a\xbf0\xb6\xc1\t\x93\x12|0+1\x9b\xfaU}+\x06\xa1\xe9\xe7\x87\r\xe6\x15\x9d\x01'
        enc = base64.b64decode(enc)
        iv = 16 * '\x00'.encode()
        cipher = AES.new(private_key, AES.MODE_CBC, iv)
        return unpad(cipher.decrypt(enc[16:]))

    def is_os_64bit(self):
        return platform.machine().endswith('64')

    def read_counter(self, data_path):
        return loads(open(data_path + "/counter.json", "r").read()) + 1 if path.exists(
            data_path + "/counter.json") else 0

    def write_counter(self, data_path, counter):
        with open(data_path + "/counter.json", "w") as f:
            f.write(dumps(counter))

    def setup(self, counter, data_path, pad):
        if counter == 0:
            cred_r = open(data_path + "/cred.txt", "r")
            cred = list(map(lambda line: line.strip(), cred_r.readlines()))
            cred_r.close()
            content = {"address": cred[0], "username": cred[2], "password": cred[3],
                       "ip": cred[1]}
            with open(data_path + "/cred.json", "w") as f:
                j_content = json.dumps(content)
                enc = self.encrypt(j_content, pad)
                f.write(enc.decode())
            os.remove(data_path + "/cred.txt")

    def get_file_name(self):
        current_time = datetime.datetime.now()
        architecture = platform.architecture()[0]
        computer_name = socket.gethostname()
        underline = "_"
        hash = "#"
        local_file_name = computer_name + hash + str(current_time.year) + underline + str(
            current_time.month) + underline + str(current_time.day)
        return local_file_name

    ###################################################xml transformation

    def list_files(self, directory, extension):
        saved = getcwd()
        chdir(directory)
        it = glob('*.' + extension)
        chdir(saved)
        return it

    def remove_extra_characters(self, vendor, name):
        flag = re.search('-?[0-9]*\.[0-9]*\.?[0-9]*\.?[0-9]*\.?[0-9]*', name)

        if vendor in name and flag:
            return re.sub('-?[0-9]*\.[0-9]*\.?[0-9]*\.?[0-9]*\.?[0-9]*', '', name.replace(vendor, ""))
        elif flag:
            return re.sub('-?[0-9]*\.[0-9]*\.?[0-9]*\.?[0-9]*\.?[0-9]*', '', name)
        elif vendor in name:
            return name.replace(vendor, "")
        else:
            return name

    def set_special_cpe(self, vendor, name, title):
        if name == "mysql_server":
            return "oracle", "mysql"
        elif "kernel" == name:
            return "linux", "linux_kernel"
        elif "acl" in name:
            return "acl_project", "acl"
        elif "gpg" in name:
            return "gnupg", "gpg_pubkey"
        elif "kdelibs" in name:
            return "kde", "kdelibs"
        elif "systemd" in name:
            return "lennart_poettering", "systemd"
        elif "policycoreutils" in name:
            return "selinuxproject", "policycoreutils"
        elif "firefox" in name:
            return "mozilla", "firefox"
        elif "putty" in name:
            return "simon", "putty"
        elif "notepad++" in name:
            return "notepad++", "notepad++"
        elif "xampp" in name:
            return "xampp", "xampp"
        elif "apache2" == name:
            return "apache", "http_server"
        elif "tomcat" == name:
            return "apache", "tomcat"
        elif "bash" == name:
            return "gnu", "bash"
        elif "openjdk" in name and "java" in name:
            return "oracle", "jdk"
        elif "wireshark" in vendor:
            return "wireshark", "wireshark"
        elif len(re.findall("postgresql_[0-9]+",name))>0:
            return "postgresql", "postgresql"
        elif "postgresql-server" in name:
            return "postgresql-server", "postgresql-server"
        elif "mariadb-server" == name:
            return "mariadb", "mariadb"
        elif "tonec" in vendor:
            return "tonec_inc", "internet_download_manager"
        elif "dreamweaver" in name:
            return "adobe", "dreamweaver"
        elif "phpstorm" in name:
            return "jetbrains", "phpstorm"
        elif "intellij" in name:
            return "jetbrains", "intellij_idea"
        elif "chrome" in name and "google" in name:
            return "google", "chrome"
        elif "fortiweb" in name:
            return "fortinet", "fortiweb"
        elif "firepower" and "center" in name and "cisco" in vendor:
            return "cisco", "firepower_managment_center"
        elif "firepower" and "threat" in name:
            return "cisco", "firepower_threat_defense"
        elif "cisco" in vendor:
            if "ios" in name.lower():
                return "cisco", "ios"
            else:
                return vendor, name
        elif "mdaemon" in name.lower():
            return "altn", "mdaemon"
        elif "asp" and ".net" in name.lower():
            return "microsoft", "asp.net"
        elif "nginx" in name:
            return "nginx", "nginx"
        elif "openssh" in name:
            return "openbsd", "openssh"
        elif "iis" in name:
            return "microsoft", "internet_information_server"
        elif len(re.findall("php[0-9]+\.[0-9]+", title))>0:
            return "php", "php"
        elif "mongodb" in name:
            return "mongodb", "mongodb"
        else:
            return vendor, name

    def fix_architecture_format(self, name):
        flag = re.search(r"(\([0-9][0-9]-bit\))|(\(?x[0-9][0-9]\)?)|(\(\w\))", name)
        if flag:
            return re.sub(r"(\([0-9][0-9]-bit\))|(\(?x[0-9][0-9]\)?)|(\(\w\))", "", name)
        else:
            return name

    def get_sql_server_cpe(self, name, version):
        version_first_segment = version.split(".")
        year = "*"
        sp = "*"
        rev = "*"
        if version_first_segment[0] == "14":
            year = "2017"
            return "cpe:2.3:a:microsoft:sql_server:{}:*:*:*:*:*:*".format(year)
        if version_first_segment[0] == "13":
            year = "2016"
            if int(version_first_segment[2]) >= 5026:
                sp = "sp2"
            else:
                sp = "sp1"
            return "cpe:2.3:a:microsoft:sql_server:{}:{}:*:*:*:*:*".format(year, sp)
        if version_first_segment[0] == "12":
            year = "2014"
            if int(version_first_segment[2]) >= int(6024):
                sp = "sp3"
            elif int(version_first_segment[2]) < int(5000) and int(version_first_segment[2]) >= int(4100):
                sp = "sp2"
            elif int(version_first_segment[2]) < int(6024) and int(version_first_segment[2]) != 2000:
                sp = "sp1"
            return "cpe:2.3:a:microsoft:sql_server:{}:{}:*:*:*:*:*".format(year, sp)

        if version_first_segment[0] == "11":
            year = "2012"
            if int(version_first_segment[2]) >= int(7001):
                sp = "sp4"
            elif int(version_first_segment[2]) < int(7001) and int(version_first_segment[2]) >= int(6020):
                sp = "s3"
            elif int(version_first_segment[2]) < int(6020) and int(version_first_segment[2]) >= 5058:
                sp = "sp2"
            elif int(version_first_segment[2]) < int(5058) and int(version_first_segment[2]) >= 3000:
                sp = "sp1"
            return "cpe:2.3:a:microsoft:sql_server:{}:{}:*:*:*:*:*".format(year, sp)

    def get_cplus_cpe(self, name, vendor, version):
        if "visual" and "redistributable" in name:

            year = re.findall(r"20[0-9][0-9]", name)
            if len(year) == 0:

                return "cpe:2.3:a:microsoft:visual_c++:*:*:*:*:*:*:*"
            else:
                sp = ""
                if year[0] == "2008" or year[0] == "2005":
                    sp = "sp1"

                return "cpe:2.3:a:microsoft:visual_c++:{}:{}:*:*:*:*:*".format(year[0], sp)
        elif "visual" and "runtime" in name:

            year = re.findall(r"20[0-9][0-9]", name)
            if len(year) == 0:

                return "cpe:2.3:a:microsoft:visual_c++:*:*:*:*:*:*:*"
            else:
                return "cpe:2.3:a:microsoft:visual_c++:{}:*:*:*:*:*:*".format(year[0])
        else:

            return "cpe:2.3:a:{}:{}:{}:*:*:*:*:*:*".format(vendor, name, version)

    def prettify_name(self, name):
        if name is not " " and name is not None and name is not "":
            name = name.replace(" ", "")
            name = name.replace("-", "")
            name = name.replace("__", "_")
            name = name.replace(")", "")
            name = name.replace("(", "")

            return name
        else:
            return "*"

    def fast_scan_alteration(self, name, version):
        if "openssh" in name:
            fixed_ver = version.split(".")
            if len(fixed_ver) == 2:
                fixed_ver = fixed_ver[0][-1] + "." + fixed_ver[1][0]
                if fixed_ver is not None:
                    return fixed_ver
            else:
                return version
        elif "http_server" in name:

            fixed_ver = version.split(".")
            if len(fixed_ver) == 3:
                fixed_ver = fixed_ver[0] + "." + fixed_ver[1] + "." + fixed_ver[2][0:2]
                if fixed_ver is not None:
                    return fixed_ver
            else:
                return version
        elif "nginx" in name:
            fixed_ver = version.split(".")
            if len(fixed_ver) == 3:
                fixed_ver = fixed_ver[0] + "." + fixed_ver[1] + "." + fixed_ver[2][0]
                if fixed_ver is not None:
                    return fixed_ver
            else:
                return version
        elif "firefox" in name:
            fixed_ver = version.split("+")
            if fixed_ver is not None:
                return fixed_ver[0]
            else:
                return version
        elif len(re.findall(r"postgresql_[0-9]+",name))>0:
            fixed_ver = version.split(".")
            if len(fixed_ver) >= 2:
                return fixed_ver[0] + "." + fixed_ver[1]
            else:
                return version
        elif name == "python":
            fixed_ver = version.split("-")
            if len(fixed_ver) == 2:
                return fixed_ver[0]
            else:
                return version
        elif name == "bash":
            fixed_ver = version.split(".")
            if len(fixed_ver) >= 2:
                return fixed_ver[0] + "." + fixed_ver[1][0]
            else:
                return version
        elif "mysql" in name and "server" in name:
            fixed_ver = version.split(".")
            if len(fixed_ver) == 3:
                fixed_ver = fixed_ver[0] + "." + fixed_ver[1] + "." + fixed_ver[2]
                return fixed_ver
            else:
                return version
        else:
            return version

    def iterate_xml(self, entry, root, cpe_item_dictionary, cpe_titile):
        for i in range(entry):
            if root[i][1][2].text is None or root[i][1][0].text is None or root[i][1][1].text is None:
                root[i][1][2].text = "*"
                root[i][1][0].text = "*"
                root[i][1][1].text = "*"

            if root[i][1][2].text is not None and root[i][1][0].text is not None and root[i][1][1].text is not None:
                vendor = root[i][1][2].text.partition(' ')[0].lower()
                if vendor == "the" or vendor == "The":
                    test = root[i][1][2].text.partition(' ')[2].lower()
                    vendor = test.partition(' ')[0].lower()

                cpe_name = self.remove_extra_characters(vendor, root[i][1][0].text.lower().replace("-", "_"))
                cpe_name = self.fix_architecture_format(cpe_name)
                cpe_name = self.prettify_name(cpe_name)
                if "c++" not in cpe_name:
                    if cpe_name.lower() != "sql_server":
                        if cpe_name.lower() != "python":
                            if "openjdk" not in cpe_name.lower():
                                if "postgresql"!=cpe_name.lower():
                                    cpe_data = self.set_special_cpe(vendor, cpe_name, root[i][1][0].text)
                                    final_cpe_name = cpe_data[1]
                                    final_cpe_vendor = cpe_data[0]
                                    final_cpe_version = root[i][1][1].text
                                    if final_cpe_name is not None and final_cpe_vendor is not None and final_cpe_version is not None:
                                        final_cpe_vendor = final_cpe_vendor.strip("_")
                                        if ":" in final_cpe_version:
                                            final_cpe_version = final_cpe_version[final_cpe_version.index(":")+1:]
                                        if "-" in final_cpe_version:
                                            final_cpe_version = final_cpe_version[0:final_cpe_version.rindex("-")]
                                        if "ubuntu" in final_cpe_version:
                                            final_cpe_version = final_cpe_version[:final_cpe_version.find("ubuntu")]
                                            if "ubuntu" in final_cpe_version:
                                                final_cpe_version = final_cpe_version[:final_cpe_version.find("ubuntu")]
                                        if "+" in final_cpe_version:
                                            final_cpe_version = final_cpe_version[:final_cpe_version.index("+")]
                                        if "~" in final_cpe_version:
                                            final_cpe_version = final_cpe_version[:final_cpe_version.index("~")]
                                        final_cpe_version = final_cpe_version.split(".")
                                        if "chrome" in final_cpe_name and len(final_cpe_version) == 4:
                                            final_cpe_version = final_cpe_version[0] + "." + final_cpe_version[
                                                1] + "." + \
                                                                final_cpe_version[2] + "." + final_cpe_version[3]
                                        elif len(final_cpe_version) == 3:
                                            final_cpe_version = final_cpe_version[0] + "." + final_cpe_version[
                                                1] + "." + \
                                                                final_cpe_version[2]
                                        elif len(final_cpe_version) == 2:
                                            final_cpe_version = final_cpe_version[0] + "." + final_cpe_version[1]
                                        else:
                                            final_cpe_version = root[i][1][1].text
                                        final_update="*"
                                        if "p" in final_cpe_version:
                                            final_update="p1"
                                        else:
                                            final_update="*"
                                        fixed_ver = self.fast_scan_alteration(final_cpe_name, final_cpe_version)

                                        cpe_item_dictionary.append(
                                            "cpe:2.3:a:{}:{}:{}:{}:*:*:*:*:*:*".format(

                                                final_cpe_vendor,
                                                final_cpe_name,
                                                fixed_ver,final_update))
                                        cpe_titile.append(root[i][1][0].text)
                                    else:
                                        cpe_item_dictionary.append(
                                            "cpe:2.3:a:{}:{}:{}:*:*:*:*:*:*:*".format(

                                                final_cpe_vendor,
                                                final_cpe_name,
                                                final_cpe_version))
                                    cpe_titile.append(root[i][1][0].text)


                            else:
                                final_cpe_version = root[i][1][1].text
                                if "jre" not in cpe_name.lower():
                                    jdk_ver = final_cpe_version
                                    total_ver=re.findall(r"([0-9]+(\.[0-9]+)*u[0-9]+)",jdk_ver)
                                    if len(total_ver) > 0:
                                        jdk_ver_f="1."+total_ver[0][0].split("u")[0]+".0"
                                        update="update_"+total_ver[0][0].split("u")[1]
                                        cpe_item_dictionary.append(
                                        "cpe:2.3:a:{}:{}:{}:{}:*:*:*:*:*:*".format(
                                            "oracle",
                                            "jdk",
                                            jdk_ver_f,
                                            update
                                            )
                                        )
                                        cpe_titile.append(root[i][1][0].text)
                                    elif len(re.findall(r"[0-9]+(\.[0-9]+)*", final_cpe_version)) > 0:
                                        jdk_ver_f=re.findall(r"[0-9]+\.[0-9]+\.[0-9]+",final_cpe_version)[0]
                                        cpe_item_dictionary.append(
                                            "cpe:2.3:a:{}:{}:{}:*:*:*:*:*:*:*".format(
                                                "oracle",
                                                "jdk",
                                                jdk_ver_f
                                            )
                                        ) 
                                        cpe_titile.append(root[i][1][0].text)
                                    else:
                                        cpe_item_dictionary.append(
                                            "cpe:2.3:a:{}:{}:{}:*:*:*:*:*:*:*".format(
                                                "oracle",
                                                "jdk",
                                                final_cpe_version
                                            )
                                        ) 
                                        cpe_titile.append(root[i][1][0].text)

                                else:
                                    jre_ver = final_cpe_version
                                    total_ver=re.findall(r"([0-9]+(\.[0-9]+)*u[0-9]+)",jre_ver)
                                    if len(total_ver) > 0:
                                        jre_ver_f="1."+total_ver[0][0].split("u")[0]+".0"
                                        update="update_"+total_ver[0][0].split("u")[1]
                                        cpe_item_dictionary.append(
                                            "cpe:2.3:a:{}:{}:{}:{}:*:*:*:*:*:*".format(
                                                "oracle",
                                                "jre",
                                                jre_ver_f,
                                                update
                                            )
                                        )
                                        cpe_titile.append(root[i][1][0].text)
                                    elif len(re.findall(r"[0-9]+(\.[0-9]+)*", final_cpe_version))>0:
                                        jre_ver_f=re.findall(r"[0-9]+\.[0-9]+\.[0-9]+", final_cpe_version)[0]
                                        cpe_item_dictionary.append(
                                            "cpe:2.3:a:{}:{}:{}:*:*:*:*:*:*:*".format(
                                                "oracle",
                                                "jre",
                                                jre_ver_f
                                            )
                                        ) 
                                        cpe_titile.append(root[i][1][0].text)
                                    else:
                                        cpe_item_dictionary.append(
                                            "cpe:2.3:a:{}:{}:{}:*:*:*:*:*:*:*".format(
                                                "oracle",
                                                "jre",
                                                final_cpe_version
                                            )
                                        ) 
                                        cpe_titile.append(root[i][1][0].text)




                        else:
                            if "-" in root[i][1][1].text:
                                rc=re.findall("-[0-9]+", root[i][1][1].text)[0]
                                rc=rc.replace("-", "")
                                version=re.findall(r"[0-9]+\.[0-9]+\.[0-9]", root[i][1][1].text)[0]
                                cpe_item_dictionary.append(
                                    "cpe:2.3:a:{}:{}:{}:{}:*:*:*:*:*:*".format("python", "python", version,"rc"+rc))
                                cpe_titile.append(root[i][1][0].text)
                            else:
                                cpe_item_dictionary.append(
                                    "cpe:2.3:a:{}:{}:{}:*:*:*:*:*:*:*".format("python", "python", root[i][1][1].text))
                                cpe_titile.append(root[i][1][0].text)

                if "sql_server" in cpe_name and "postgresql" not in cpe_name and "mysql" not in cpe_name:
                    cpe_item_dictionary.append(
                        self.get_sql_server_cpe(cpe_name, root[i][1][1].text.replace(" ", "").lower()))
                    cpe_titile.append(root[i][1][0].text)

                if "c++" in cpe_name:
                    cpe_item_dictionary.append(
                        self.get_cplus_cpe(cpe_name, vendor, root[i][1][1]))
                    cpe_titile.append(root[i][1][0].text)

    def get_os_info(self):
        proc = os.popen('lsb_release -si').read()
        if "ubuntu" in str(proc).lower():
            if self.is_os_64bit():
                ar = "x64"
            else:
                ar = "x86"

            os_version = distro.linux_distribution()
            os_name = "cpe:2.3:o:{}:{}:{}:*:*:*:*:*:*:*:{}:*".format("canonical", "ubuntu_linux", os_version[1], ar)
            os_title = "Ubuntu"
            return os_name, os_title
        elif "debian" in str(proc).lower() :
            if self.is_os_64bit():
                ar = "x64"
            else:
                ar = "x86"

            os_version = distro.linux_distribution()
            os_name = "cpe:2.3:o:{}:{}:{}:*:*:*:*:*:*:*:{}:*".format("debian", "debian_linux", os_version[1], ar)
            os_title = "Debian"
            return os_name, os_title
        else:
            return "*","*"



    def prepare_list_for_export(self, dir, cpe_item_dictionary, cpe_title, final_cpe_list, os_name, os_title,xml_file_name,data_path):
        for k in range(len(cpe_item_dictionary)):
            final_cpe_list.append({"name": cpe_item_dictionary[k], "title": cpe_title[k]})
        final_cpe_list.append({"name": os_name, "title": os_title})
        # for k in range(len(dic)):
        #    json_list.append({"name": dic[k], "title": title[k]})
        with open(data_path +"/"+ dir + ".json", "w") as f:
            # json_list.append({"name": os_name, "title":ops})

            json.dump(final_cpe_list, f)
        os.remove(data_path+"/"+xml_file_name)

    def prepare_data_for_transfer(self, dir, unpad, my_logger, final_cpe_list,data_path):
        url = ""
        #################getting user info
        try:
            with open(data_path+"/cred.json", "r") as json_data:
                content = json_data.read()
                dec = self.decrypt(content, unpad).decode()
                d = json.loads(dec)
                url = d["address"]
                ip = d["ip"]
        except:

            my_logger.critical("VMS-AGENT:CANNOT ACSSES FILE CRED.JSON.EXITING")
            sys.exit(1)

        http_list = []
        http_list.append("http://")

        http_list.append(url)
        address = "".join(http_list)
        #######################removing invalid chars
        add2 = ''
        for i in address:
            if ord(i) != 0:
                add2 = add2 + i
        ip2 = ""
        for i in ip:
            if ord(i) != 0:
                ip2 = ip2 + i
        ###############################################################scan  types
        try:
            if len(sys.argv) == 2:
                if sys.argv[1] == "--fast":
                    with open(data_path+"/chosen_cpe.json", "r") as file:
                        data = json.load(file)
                        data = list(map(lambda cpe: cpe['name'].split(':')[3:5], data))
                        chosen_cpe = list(filter(lambda item: item['name'].split(':')[3:5] in data, final_cpe_list))
                    info_dic = {"ip": ip2, "cpes": chosen_cpe, "hotfix": [], "hostname": socket.gethostname()}
                    with open(data_path+"/" + dir + "FAST" + ".json", "w") as f:
                        # json_list.append({"name": os_name, "title":ops})

                        json.dump(chosen_cpe, f)
                elif sys.argv[1] == "--deep":
                    info_dic = {"ip": ip2, "cpes": final_cpe_list, "hotfix": [], "hostname": socket.gethostname()}
            else:
                my_logger.critical("VMS-AGENT:NO SCAN TYPE ARG WAS GIVEN.EXITING")
                sys.exit(1)
            return info_dic, ip2, add2

        except IOError:
            my_logger.critical("VMS-AGENT:FILE CONTAING CPE'S NOT FOUND.EXITING")
            sys.exit(1)

    def send_file(self, file, url, list_cpe, data_path, unpad, my_logger):
        cr = []
        with open(data_path + "/cred.json", "r") as json_data:
            d_content = json_data.read()
            dec = self.decrypt(d_content, unpad).decode()
            j_dic = json.loads(dec)
            cr.append(j_dic["username"])
            cr.append(j_dic["password"])
        payload = {
            "username": cr[0],
            "password": cr[1]
        }
        headers1 = {'Content-Type': 'application/json'}

        try:
            url = requests.get(url).url
        except Exception:
            url = url.replace("http", "https")

        # r = requests.post("http://{}/api/token/".format(url), data=json.dumps(payload).replace(r"\u0000", ""), headers=headers1)
        try:
            token_r = requests.post((urljoin(url,"api/token/")).replace(r"\u0000", ""), data=json.dumps(payload),
                                    headers=headers1,
                                    timeout=180, verify=False)
            if token_r.status_code == 503:
                my_logger.critical("VMS-AGENT:SERVICE UNAVAILABLE")
            if token_r.status_code == 500:
                my_logger.critical("VMS-AGENT:INTERNAL SERVER ERROR")

            if token_r.status_code == 403:
                my_logger.critical("VMS-AGENT:FORBIDEN CONNECTION")

            if token_r.status_code == 401:
                my_logger.critical("UNTHORIZED CONNECTION")
        except requests.exceptions.ConnectionError as cn_e:
            my_logger.critical("VMS-AGENT:CONNECTION ERROR:{0}.EXITING".format(cn_e))
            sys.exit(1)

        except requests.exceptions.Timeout as ti_e:
            my_logger.critical("VMS-AGENT:TIMEOUT ERROR:{0}.EXITING".format(ti_e))
            sys.exit(1)

        except requests.exceptions.HTTPError as ht_e:
            my_logger.critical("VMS-AGENT:HTTP ERROR:{0}.EXITING".format(ht_e))
            sys.exit(1)

        respnse = token_r.json()

        headers2 = {'Content-type': 'application/json', 'Authorization': 'Bearer {}'.format(respnse["access"])}

        with open(file, 'r') as file:
            j_f = json.load(file)

            # r = requests.post("http://{}/api/agentcall/".format(url), json=list, headers=headers2)
        try:
            r = requests.post((urljoin(url,"api/agentcall/")).replace(r"\x00", ""), json=list_cpe, headers=headers2,
                              timeout=180,
                              verify=False)
            if r.status_code == 503:
                my_logger.critical("VMS-AGENT:SERVICE UNAVAILABLE")
            if r.status_code == 500:
                my_logger.critical("VMS-AGENT:INTERNAL SERVER ERROR")

            if r.status_code == 403:
                my_logger.critical("VMS-AGENT:FORBIDEN CONNECTION")

            if r.status_code == 401:
                my_logger.critical("VMS-AGENT:UNTHORIZED CONNECTION")

        except requests.exceptions.ConnectionError as cn_e:
            my_logger.critical("VMS-AGENT:CONNECTION ERROR:{0}.EXITING".format(cn_e))
            sys.exit(1)

        except requests.exceptions.Timeout as ti_e:
            my_logger.critical("VMS-AGENT:TIMEOUT ERROR:{0}.EXITING".format(ti_e))
            sys.exit(1)

        except requests.exceptions.HTTPError as ht_e:
            my_logger.critical("VMS-AGENT:HTTP ERROR:{0}.EXITING".format(ht_e))
            sys.exit(1)
        if r.status_code == 200:
            my_logger.info("VMS-AGENT:DATA SENT SUCESSFULLY")
    def xml_fixer(self,data_path):
            os.rename(data_path+"/pkg.xml",data_path+"/pkg.txt")
            file=open(data_path+"/pkg.txt","r")
            data=file.read()
            def xml_escaper(match):
                    data=match.group('data')
                    esc_dict = {
                    "<" : "&lt;",
                    ">" : "&gt;",
                    '"' : "&quot;",
                    "'" : "&apos;",
                    "&" : "&amp;"
                    }
                    return "{}{}{}".format(match.group('st'),
                           re.sub(r"[<>\"'&]",lambda x: esc_dict[x.group(0)] ,data),
                           match.group('en'))
            new_data=re.sub(r"(?P<st><S.*?>)(?P<data>.*?[<>\"'&].*?)(?P<en></S>)", xml_escaper, data)
            new_file=open(data_path+"/pkg.xml","w")
            new_file.write(new_data)
            if os.path.exists(data+"/pkg.txt"):
                os.remove(data_path+"/pkg.txt")


    def start(self):
        urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
        my_logger = logging.getLogger("vms-agent")
        my_logger.setLevel(logging.DEBUG)
        handler = logging.handlers.SysLogHandler(address="/dev/log")
        my_logger.addHandler(handler)
        my_logger.info("VMS-AGENT:AGENT STARTED")
        data_path = "/var/vms-agent"
        if not os.path.exists(data_path):
            os.mkdir(data_path)
        BLOCK_SIZE = 16
        pad = lambda s: s + (BLOCK_SIZE - len(s) % BLOCK_SIZE) * chr(BLOCK_SIZE - len(s) % BLOCK_SIZE)
        unpad = lambda s: s[:-ord(s[len(s) - 1:])]
        counter = self.read_counter(data_path)
        atexit.register(self.write_counter, data_path, counter)
        self.setup(counter, data_path, pad)
        self.xml_fixer(data_path)
        if os.path.exists(data_path + "/data.txt"):
            os.remove(data_path + "/data.txt")
        if os.path.exists(data_path + "/pkg.txt"):
            os.remove(data_path + "/pkg.txt")
        if os.path.exists(data_path + "/packages.xml"):
            os.remove(data_path + "/packages.xml")

        pkg_xml = minidom.parse(data_path + "/pkg.xml")

        pkg_xml_items = pkg_xml.getElementsByTagName('Obj')
        final_cpe_list = []

        cpe_item_dcitionary = []
        cpe_title = []
        pkg_parsed_xml = ET.parse(data_path + "/pkg.xml")
        pkg_xml_root = pkg_parsed_xml.getroot()
        self.iterate_xml(len(pkg_xml_items), pkg_xml_root, cpe_item_dcitionary, cpe_title)
        dir = self.get_file_name()
        os_name, os_title = self.get_os_info()
        self.prepare_list_for_export(dir, cpe_item_dcitionary, cpe_title, final_cpe_list, os_name, os_title,
                                     "pkg.xml",data_path)
        info_dic, final_ip, final_address = self.prepare_data_for_transfer(dir, unpad, my_logger, final_cpe_list,data_path)
        self.send_file(data_path + "/" + dir + ".json", final_address, info_dic, data_path, unpad, my_logger)


######################################call send


