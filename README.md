## **Overview**

This is a **Vulnerability Management Solution** (VMS) agent that will gather the installed packages on your server/pc and reformat to a **CPE** format and send it to the specified server for later manipulation.

## **Features**
- [x] Packages Gathering
- [x] CPE Formation
- [x] Server Token Authentication
- [x] Server Credential Authentication
- [x] Local Data
- [x] Fast Scan
- [x] Deep Scan
- [x] Changable Run Cycle

## **Requirements**
* **pip 2**
* **virtutalenv**
* **python => 2.7**
* **gcc library**
* **python2-devel**

## **Getting started**
* to get started for the first time run setup.sh as **SUPERUSER**.
* when running the setup.sh file give the required args as such :
#### for a deep scan of your system
```bash
sudo setup -d --deep
```
###### this will scan your whole system for installed packages
#### for a fast scan of your system
```bash
sudo setup -d --fast
```
###### this will scan your your system based on the provided chosen_cpe.json file
* After  starting the script the dependencies alongside a virtual env will be installed
* After instalation you will be asked to enter :
    1. Designated IP For This Device
    2. The Server Address
    3. Username
    4. Password
* After this instalation is complete and the agent will run periodically(default is every 12h)

## **Notes**
* the initial folder have some files and dependencies in it.**DO NOT REMOVE OR ALTER FOLDER CONTENTS IN ANY WAY**.<br>
* you can change the contents of chosen_cpe.json file to be used in fast scan,but remember the entry's must be in **CPE** format as shown in <a href=https://nvd.nist.gov>The Nist</a>
* If you wnat to start the agent manually  run the following command
```bash
sudo /var/vms-agent/agen_start.sh
```
* If you want to start fresh and change the input values run the fresh_start.sh script as **SUPERUSER**
* the default directory for the local file of the gathered cpe's is _**/var/vms-agent**_<br>
<br>
> NEGIN PARDAZESH FARDA
